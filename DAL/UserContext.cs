﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.ModelConfiguration;
using Microsoft.AspNet.Identity.EntityFramework;
using learning_asp.net.Models;

namespace learning_asp.net.DAL
{
    /*
        Essa classe cordena as funcionalidades do Entity Framework para um determinado model

         */
    public class UserContext : DbContext
    {
        // SqlServer é o nome da strign de conexão configurada no arquivo web.config
        public UserContext() : base("UserContext")
        {

        }

        //Representa uma tabela no banco de dados 
        public DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();

            modelBuilder.Configurations.Add(new EntityTypeConfiguration<User>());
        }
    }

}
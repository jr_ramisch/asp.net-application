﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using learning_asp.net.Models;


namespace learning_asp.net.Controllers
{
    public class HomeController : Controller
    {
        /* ViewData só transfere informação da Action method pra View
           As informações só são válidas durante a requisição atual
           ViewData precisa de type casting, ViewData é um dicionario
    
           ViewBag é uma "dynamic property", não precisa de type casting,
           usa as propriedades do model como meio de acesso as informações
           na View. 

           Strongly Typed View model que agrupa outros models e pra View assim -> View(model)

           TemData é um dicionário, 
        */
        public ActionResult Index(int id = 1)
        {
            UserBusinessLayer userBussinesLayer = new UserBusinessLayer();
            User user = userBussinesLayer.getUserDetails(id);
            //ViewData["User"] = user;
            //ViewData["Header"] = "User details";
            
            ViewBag.userDetails = user;
            //ViewBag.header = "detalhes do usuario";

            return View(user);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
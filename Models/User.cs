﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace learning_asp.net.Models
{
    public class User
    {
        public int id { set; get; }
        public string name { set; get; }
        public string address { set; get; }
        public string password { set; get; }
        public string username { set; get; }

    }
}
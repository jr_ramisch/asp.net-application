﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using learning_asp.net.Models;

namespace learning_asp.net.Repository
{
    public class ApplicationRepository
    {
        public SqlConnection getDataBaseConnection()
        {
            string connection = ConfigurationManager.ConnectionStrings["SqlServer"].ConnectionString;
            return new SqlConnection(connection);
        }


    }

    
}